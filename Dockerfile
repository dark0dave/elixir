FROM elixir:1.11.2-alpine as build

WORKDIR /app

ADD mix.exs mix.lock ./

RUN mix local.hex --force && \
  mix local.rebar --force && \
  mix deps.get --only prod

ADD lib lib
ADD priv priv
ADD test test
ADD config config

RUN MIX_ENV=prod mix release

FROM alpine:latest

ENV LANG=C.UTF-8

RUN apk update && apk add openssl ncurses-libs

WORKDIR /app

COPY --from=build /app/_build/prod/ .

CMD ["./rel/books_api/bin/books_api", "start"]
