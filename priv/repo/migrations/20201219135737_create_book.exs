defmodule BooksApi.Repo.Migrations.CreateBook do
  use Ecto.Migration

  def change do
    create table(:book, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :author, :string

      timestamps()
    end

  end
end
